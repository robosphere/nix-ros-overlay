
# Copyright 2023 Open Source Robotics Foundation
# Distributed under the terms of the BSD license

{ lib, buildRosPackage, fetchurl, ament-cmake, ament-cmake-gtest, ament-lint-auto, ament-lint-common, boost, nao-lola-command-msgs, nao-lola-sensor-msgs, rclcpp }:
buildRosPackage {
  pname = "ros-rolling-nao-lola-client";
  version = "1.1.0-r2";

  src = fetchurl {
    url = "https://github.com/ros2-gbp/nao_lola-release/archive/release/rolling/nao_lola_client/1.1.0-2.tar.gz";
    name = "1.1.0-2.tar.gz";
    sha256 = "6609c54654e6b33ed42e10d23871a0740f5713005a3ae2409871505021df9524";
  };

  buildType = "ament_cmake";
  buildInputs = [ ament-cmake ];
  checkInputs = [ ament-cmake-gtest ament-lint-auto ament-lint-common ];
  propagatedBuildInputs = [ boost nao-lola-command-msgs nao-lola-sensor-msgs rclcpp ];
  nativeBuildInputs = [ ament-cmake ];

  meta = {
    description = ''Packages that allow communicating with the NAO's Lola middle-ware.'';
    license = with lib.licenses; [ asl20 ];
  };
}
